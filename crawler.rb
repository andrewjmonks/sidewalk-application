require 'anemone'
require 'data_mapper'
require 'nokogiri'
require 'yaml'

# clearly your real database isn't in-memory sqlite, but it works for a demo
DataMapper.setup(:default, 'sqlite::memory:')

class Business
	include DataMapper::Resource

	# I haven't added scrapers for all of these fields yet
	property :name, String, :key => true # Sidewalk's database definitely doesn't actually key by name, but since I'm to assume you have a fancy matching algorithm I'm gonna skip matching and just match by name
	property :description, Text
	property :tagline, String
	property :created_at, DateTime
	property :url, String
	property :tag_list, Text
	property :phone_number, String
	property :lat, Float
	property :lng, Float
	property :street_address1, String
	property :street_address2, String
	property :hours, String
	property :postal_code_id, String
end

DataMapper.finalize
DataMapper.auto_upgrade!



# this crawl is RIDICULOUS and will try to get through google's entire website
Anemone.crawl("https://www.google.com/offers/?gl=US") do |anemone|
	anemone.on_every_page do |page|
		# check for google offers place description box
		if page.doc.at_css('div.mgoh-a-address')
			puts page.url
			# since this is for a business, add one to the database (or load it if it already exists)
			# @business = Sidewalk.match_or_import(:name => page.doc.at_xpath("//span[@itemprop='name']").inner_html)
			@business = Business.first_or_create(:name => page.doc.at_xpath("//span[@itemprop='name']").inner_html)
			@business.created_at =  Time.now

			# get latitude
			if page.doc.at_xpath("//meta[@itemprop='latitude']")
				@business.lat = page.doc.at_xpath("//meta[@itemprop='latitude']/@content")
			end
			# get longitude
			if page.doc.at_xpath("//meta[@itemprop='longitude']")
				@business.lng = page.doc.at_xpath("//meta[@itemprop='longitude']/@content")
			end
			# get url
			if page.doc.at_xpath("//a[@itemprop='url']")
				@business.url = page.doc.at_xpath("//a[@itemprop='url']/@href")
			end
			# get address 1
			if page.doc.at_xpath("//meta[@itemprop='streetAddress']")
				@business.street_address1 = page.doc.at_xpath("//meta[@itemprop='streetAddress']/@content")
			end
			# should add address line 2; the reference business i was using didn't have one
			# get zip code
			if page.doc.at_xpath("//meta[@itemprop='postalCode']")
				@business.postal_code_id = page.doc.at_xpath("//meta[@itemprop='postalCode']/@content")
			end
			# get telephone
			if page.doc.at_xpath("//span[@itemprop='telephone']")
				@business.phone_number = page.doc.at_xpath("//span[@itemprop='telephone']").inner_html
			end

			# check for google + local page
			if page.doc.at_css("div.mgoh-a-address").inner_html.include? "View Google+ Local Page"
				# get g+ local page; the https switch is cuz redirects mess with open-uri
				@localpageurl = page.doc.at_xpath("//a[contains(text(), 'View Google+ Local Page')]/@href").to_s.gsub("http://","https://")
				@localpage = Nokogiri::HTML(open(@localpageurl))
				# check for hours (currently only gets today's hours)
				if @localpage.at_css('span.pja')
					@business.hours = @localpage.at_css('span.pja').inner_html
				end
				# check for description
				if @localpage.at_css('div.Jya')
					@business.description = @localpage.at_css('div.Jya').inner_html
				end
			end

			@business.save

			# I guess yaml's kinda old school, but since we're populating a database this output is effectively just for testing
			puts @business.to_yaml + "\n"
		else 
			puts "\n not an offer"
			puts page.url
		end
	end
end