# placesclient.rb

require 'open-uri'
require 'nokogiri'
require 'yaml'

class Place
	# I could add more fields but this gets the point across
	attr_accessor :name, :lat, :lng

	def initialize(name,lat,lng)
		@name = name
		@lat = lat
		@lng = lng
	end

	def self.search_google_places(lat,long,keyword)
		# make array for results
		@result_collector = []

		# load first page of results
		# currenlty sorts by distance but if you add "&radius=50000" and remove "&rankby=distance" it'll show every place within 50,000 meters
		@url =  "https://maps.googleapis.com/maps/api/place/nearbysearch/xml?location=" + lat.to_s + "," + long.to_s + "&keyword=" + keyword + "&rankby=distance&sensor=false&key=AIzaSyDIPa8VjR1Iybo-vl60dd3CpSmw2zyMfvA"
		@doc = Nokogiri::HTML(open(@url))

		# until you're on the last page
		until @doc.at_xpath("//next_page_token") == nil
			# open the next page; this is redundant the first time which I should fix by indexing the loop
			@doc = Nokogiri::HTML(open(@url))
			# get each place result
			all_results = @doc.xpath("//result")
			# and loop through them
			all_results.each_with_index do |result,index|
				# adding name, lat, and lng. I should add more fields.
				@name = result.xpath("//name")[index].inner_html
				@lat = result.xpath("//lat")[index].inner_html
				@lng = result.xpath("//lng")[index].inner_html
				# create the object
				@place = Place.new(@name,@lat,@lng)
				# and add it to the results array
				@result_collector << @place
			end
			# if there's another page
			if @doc.at_xpath("//next_page_token")
				puts "found another page"
				@next_page_token =  @doc.at_xpath("//next_page_token").inner_html
				# load that other page
				@url = "https://maps.googleapis.com/maps/api/place/nearbysearch/xml?location=" + lat.to_s + "," + long.to_s + "&keyword=" + keyword + "&radius=50000&sensor=false&key=AIzaSyDIPa8VjR1Iybo-vl60dd3CpSmw2zyMfvA&pagetoken=" + @next_page_token
				puts @url
				# and give the API some time to recover (a shorter sleep would probably be safe too)
				sleep(5)
			end
		end
		# yaml the place list
		return @result_collector.to_yaml
	end
end

# here's an example query
puts Place.search_google_places(42.457407,-71.356115,"food")