sidewalk-application
====================

This is my code for the interview process at Sidewalk.

##	crawler.rb

crawler.rb crawls Google Offers for businesses, and scrapes their data into a database. In real life this would be the Sidewalk database, but this demo uses sqlite stored in memory which is problematic. If the business has a google+ local page, it scrapes data from that too.

##	placesclient.rb

placesclient.rb outputs an array of nearby places that match a given keyword

##	flaws

*	neither of the scripts have any unit tests