class Array
	def reverse!
		index = 0
		while index < self.length / 2
			value = self[index]
			self[index] = self[self.length - 1 - index]
			self[self.length - 1 - index] = value
			index += 1
		end
		return self
	end

	def recursive_reverse_print
		print self[length-1]
		self.pop
		until self.length == 0
			self.recursive_reverse_print
		end
	end
end

class String
	def reverse_phrases
		@backwardsString = String.new
		self.split(",").each_with_index do |phrase, index|
			@backwardsString << phrase.split(/\s+/).reverse.join(" ")
			if index != self.split(",").length - 1
				@backwardsString << ", "
			end
		end
		puts @backwardsString.capitalize
	end
end

mystring = "Hey man, can't you  Play that   guitar, please?"
mystring.reverse_phrases
# output = "Man hey, guitar that play you can't , please?"